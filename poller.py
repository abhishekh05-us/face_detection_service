import boto3
import config as cfg
import json
import io
import matplotlib.image as mpimg
import numpy
import inference

class aws():

    def sqsClient (self):
        sqs = boto3.client(
            'sqs',
            region_name = cfg.aws_region_name,
            aws_access_key_id = cfg.aws_access_key_id,
            aws_secret_access_key = cfg.aws_secret_access_key,
            endpoint_url = 'https://sqs.' + cfg.aws_region_name + '.amazonaws.com'
        )
        return(sqs)
    
    def s3Client (self):
        s3 = boto3.client(
            's3',
            region_name = cfg.aws_region_name,
            aws_access_key_id = cfg.aws_access_key_id,
            aws_secret_access_key = cfg.aws_secret_access_key
        )
        return(s3)
    
    def s3Resource (self):
        s3 = boto3.resource(
            's3', 
            region_name=cfg.aws_region_name,
            aws_access_key_id = cfg.aws_access_key_id,
            aws_secret_access_key = cfg.aws_secret_access_key
        )
        return(s3)

class poller():
    def __init__(self):
        self.sqs = aws().sqsClient()
    
    def sqs_receive_message(self):
        response = self.sqs.receive_message(
            QueueUrl=cfg.aws_sqs_queue_url,
            AttributeNames=['SentTimestamp'],
            MaxNumberOfMessages=10,
            MessageAttributeNames=['All'],
            VisibilityTimeout=30,
            WaitTimeSeconds=20
        )
        return response
    
    def sqs_delete_message(self,receipt_handle):
        self.sqs.delete_message(
            QueueUrl=cfg.aws_sqs_queue_url,
            ReceiptHandle=receipt_handle
        )
        print('Deleted message: %s' % receipt_handle)
    
    def parse_sqs_message(self,response):
        receiptHandles = []
        messageBody = []
        for message in response['Messages']:
            receiptHandles.append(message['ReceiptHandle'])
            messageBody.append(json.loads(message['Body'])['Message'])
        return receiptHandles, messageBody
class getImage():

    def __init__(self):
        self.s3 = aws().s3Resource()
    
    def s3Read(self,messageBody):
        Bucket = messageBody['bucket']
        Key = messageBody['key']
        print(Key)
        image_object = self.s3.Bucket(Bucket).Object(Key)
        image = mpimg.imread(io.BytesIO(image_object.get()['Body'].read()), 'jp2')
        return(image)
# Main fucntion
def main():
    
    # !!! Infinite while loops comes here !!!
    sqs_message = poller().sqs_receive_message()
    receiptHandles, messageBody = poller().parse_sqs_message(sqs_message)
    
    print(receiptHandles)
    print(messageBody)
    print("Read %d messages from SQS" %len(messageBody))
    
    for message,receipt in zip(messageBody,receiptHandles):
        image = getImage().s3Read(json.loads(message))
        message=json.loads(message)
        #print("THIS IS MY MESSAGE:  ",message)
        #print(image)
        fc = inference.face_detection_service()
        json_message_list=fc.bounding_predict(image,message)
         # !!! write to mongodb comes here !!!
if __name__ == "__main__":
    main()
