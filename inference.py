import sys
import time
import numpy as np
import tensorflow as tf
import cv2
import random
from os import listdir
from os.path import isfile, join
import os
import boto3
import multiprocessing
from utils import label_map_util
from utils import visualization_utils as vis_util
import uuid

class face_detection_service():
    
    def __init__(self):
        self.PATH_TO_CKPT = 'frozen_inference_graph_face.pb'
        # List of the strings that is used to add correct label for each box.
        self.PATH_TO_LABELS = 'object-detection_human_face.pbtxt'
        self.NUM_CLASSES = 1
        #self.IMG_DIR = "OID/Dataset/validation/test_images"
        #self.WRITE_DIR = "annotated_media/"
        #self.CROPPED_WRITE_DIR = "cropped_images/"
        self.label_map = label_map_util.load_labelmap(self.PATH_TO_LABELS)
        self.categories = label_map_util.convert_label_map_to_categories(self.label_map, max_num_classes=self.NUM_CLASSES, use_display_name=True)
        self.category_index = label_map_util.create_category_index(self.categories)
        self.detection_graph = tf.Graph()
        
        

    def uniqueid():
        seed = random.getrandbits(32)
        while True:
            yield seed
            seed += 1
    # Imports the default graph
    def import_graph(self):
        with self.detection_graph.as_default():
            od_graph_def = tf.compat.v1.GraphDef()
            with tf.io.gfile.GFile(self.PATH_TO_CKPT, 'rb') as fid:
                serialized_graph = fid.read()
                #print(fid.read())
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')
             
    def bounding_predict(self,image,message):
        print("--------------------------------------------------------------------------------------------------------------")
        self.import_graph()
        with self.detection_graph.as_default():
            config = tf.compat.v1.ConfigProto()
            config.gpu_options.allow_growth = True
            with tf.compat.v1.Session(graph=self.detection_graph, config=config) as sess:
                image_np = image
                (im_width, im_height, im_depth) = image.shape
                # the array based representation of the image will be used later in order to prepare the
                # result image with boxes and labels on it.
                # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
                image_np_expanded = np.expand_dims(image_np, axis=0)
                image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
                # Each box represents a part of the image where a particular object was detected.
                boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
                # Each score represent how level of confidence for each of the objects.
                # Score is shown on the result image, together with the class label.
                scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
                classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
                num_detections = self.detection_graph.get_tensor_by_name('num_detections:0')
                # Actual detection.

                start_time = time.time()
                (boxes, scores, classes, num_detections) = sess.run([boxes, scores, classes, num_detections],feed_dict={image_tensor: image_np_expanded})
                num_boxes = 0
                for i in range(100):
                    if scores[0][i] > 0.3:
                        num_boxes += 1
                #print(boxes[0][0:num_boxes])
                #print(np.squeeze(boxes))
                elapsed_time = time.time() - start_time
                #print('inference time cost: {}'.format(elapsed_time))
                list_json=[]
                image_id=message['key'].split(".")[0]
                print(num_boxes)
                inference={}
                if num_boxes==1:
                    inference["image_id"]=image_id
                    inference['face_id']=str(uuid.uuid1())
                    inference['bucket'] = message['bucket']
                    inference["box_dim"]={"x1":boxes[0][i][0],
                     "x2":boxes[0][i][1],
                     "y1":boxes[0][i][2],
                     "y2":boxes[0][i][3]
                    }
                    print(inference)
                    return inference
                else:
                    for i in range(0,len(boxes[0][0:num_boxes])):
                        #print(len(boxes[0][0:num_boxes]))
                        inference["image_id"]=image_id
                        inference['face_id']=str(uuid.uuid1())
                        inference['bucket'] = message['bucket']
                        inference["box_dim"]={"x1":boxes[0][i][0],
                         "x2":boxes[0][i][1],
                         "y1":boxes[0][i][2],
                         "y2":boxes[0][i][3]
                        }
                        list_json.append(inference)
                    print(list_json)
                    return list_json
        
                     
            
            
        def main():
            fc = face_detection_service()
            fc.predictions()

        if __name__ == "__main__":
            main()